FROM dorowu/ubuntu-desktop-lxde-vnc:bionic

# Fix dirmngr
RUN apt-get purge dirmngr -y && apt-get update && apt-get install dirmngr -y
RUN apt-get dist-upgrade -y

# Adding keys for ROS
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# Installing ROS
RUN apt-get update && apt-get install -y ros-melodic-desktop-full wget nano python-rosdep
RUN apt-get install -y \
      ros-melodic-libfranka python-catkin-tools libeigen3-dev 
RUN apt-get install -y python-rosinstall python-rosinstall-generator python-wstool build-essential
RUN apt-get install vim -y
RUN rosdep init && rosdep update

RUN mkdir -p /root/ws_tmp /root/Desktop && ln -s /usr/share/applications/lxterminal.desktop /root/Desktop/lxterminal.desktop
ENV ROS_DISTRO=melodic

RUN /bin/bash -c "echo -e 'umask 000\n \
      source /opt/ros/melodic/setup.bash\n \
      cd /root/Desktop/ros_ws/\n' >> /root/.bashrc "

WORKDIR /root/ws_tmp
COPY vnc/copyws.sh /root/copyws.sh
RUN chmod a+x /root/copyws.sh
COPY ./src /root/ws_tmp/src

RUN apt-get update && rosdep install --from-paths . -r -y

RUN catkin config \
      --extend /opt/ros/melodic
ENV OPENBOX_ARGS --startup "/root/copyws.sh"
VOLUME /root/Desktop/ros_ws
